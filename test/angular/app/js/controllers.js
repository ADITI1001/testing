'use strict';

angular.module('xenon.controllers', []).
	controller('LoginCtrl', ["$scope", "$rootScope", "$state", "Auth", "userProfile", function( $scope, $rootScope, $state, Auth, userProfile)
	{
		$scope.credentials = {
			username : '',
			password : ''
		};

/*		$scope.login = function (credentials) {
		    AuthService.login(credentials).then(function (data) {
		    	$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
		    	$scope.setCurrentUser(data);
		    }, function () {
				$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
		    });
		};*/
		$rootScope.isLoginPage        = true;
		$rootScope.isLightLoginPage   = false;
		$rootScope.isLockscreenPage   = false;
		$rootScope.isMainPage         = false;
		$scope.login = function () {
			Auth.login($scope.credentials).then(function () {
		  		return userProfile.$refresh();
			}).then(function () {
			  $rootScope.name = userProfile.fname +" "+userProfile.lname;
			  $rootScope.role = userProfile.roles;
			  $rootScope.uid = userProfile.uid;
			  toastr.success($rootScope.name, 'Welcome!', {timeOut: 5000});
			  $state.go("app.variant-4");
			}).catch(function(error){
				toastr.error('You have entered wrong email/password', 'Sorry!', {timeOut: 5000});
			})
		};
	}]).
	controller('NavBars', ["$scope", "$http", "userProfile", function($scope, $http, userProfile)
	{
		$scope.username = userProfile.fname;
		alert($rootScope.title);
	}]).
	controller('MyPage',function($scope, $http, $rootScope)
	{
		$scope.addnew = {};

		$scope.RegisterNewUser = function(){
			var newuser = $scope.addnew;
			console.log(newuser);
			$http({
				method : 'POST',
				url    : '/RegisterNewUser',
				contentType: 'application/x-www-form-urlencoded',
				data:  JSON.stringify(newuser)
			})
			.then(function(response){
				console.log(response.data);
			})
		}
	}).
	controller('Logout', ["$scope", "$http", "$window", function($scope, $http, $window){
		$window.location.reload();
	}]).

	controller('Dashboard', ["$scope", "$http","$rootScope", function($scope, $http, $rootScope){
		var user_id ={}
		user_id.uid = $rootScope.uid

		$scope.manpwrData;
		$scope.manPwr = {}
		$scope.dashProjSmry = [];
		$scope.dashProjStatus = [];
		console.log(user_id)
			$http({
				method : 'POST',
				url    : '/dash-project-summary',
				contentType: 'application/x-www-form-urlencoded',
				data:  JSON.stringify(user_id)
			})
			.then(function(response){
				console.log(response.data);
				$scope.dashProjSmry=response.data;
				console.log($scope.dashProjSmry);
			})


			$http({
				method : 'POST',
				url    : '/dash-claim-summary',
				contentType: 'application/x-www-form-urlencoded',
				data:  JSON.stringify(user_id)
			})
			.then(function(response){
				console.log(response.data);
			})


			$http({
				method : 'POST',
				url    : '/dash-manpower-details',
				contentType: 'application/x-www-form-urlencoded',
				data:  JSON.stringify(user_id)
			})
			.then(function(response){
				console.log(response.data);
				$scope.manpwrData = response.data;
				$scope.manpwrData.forEach(function(item){
					var roleid = item.role_id.substring(0, 2).toLowerCase();
					$scope.manPwr.project_id = item.project_id;
					$scope.manPwr.project_name = item.project_name;
					console.log(roleid)
					if(roleid == "gm"){
						var obj = item
						$scope.manPwr.gmcnt = obj.cnt;
						obj.gmcnt = obj.cnt
						obj.crcnt = ""
						obj.fecnt = ""
						obj.admincnt = ""
						console.log(obj)
						delete obj.cnt;
					}else if(roleid == "cr"){
						var obj = item
						console.log(obj)
						$scope.manPwr.crcnt= obj.cnt;
						obj.crcnt = obj.cnt
						obj.gmcnt = ""
						obj.fecnt = ""
						obj.admincnt = ""
						delete obj.cnt;
					}else if(roleid == "fe"){
						var obj = item
						console.log(obj)
						$scope.manPwr.fecnt= obj.cnt;
						obj.fecnt = obj.cnt
						obj.gmcnt = ""
						obj.crcnt = ""
						obj.admincnt = ""
						delete obj.cnt;
					}else{
						var obj = item
						console.log(obj)
						$scope.manPwr.admincnt= obj.cnt;
						obj.admincnt = obj.cnt
						obj.gmcnt = ""
						obj.crcnt = ""
						obj.fecnt = ""
						delete obj.cnt;
					}
					console.log($scope.manpwrData)
				})
			})

			$http({
				method : 'POST',
				url    : '/dash-project-status',
				contentType: 'application/x-www-form-urlencoded',
				data:  JSON.stringify(user_id)
			})
			.then(function(response){
				console.log(response.data);
				$scope.dashProjStatus = response.data;
			})

	}]).

	controller('NodalRelease', ["$scope", "$http", "$window", "$rootScope", "$state", function($scope, $http, $window, $rootScope, $state){
		$scope.finance = {
			pid : '',
			div_id : '',
			tkc_id : '',
			addrs : '',
			ramount : '',
			date : '',
			user :'',
			claim:'',
			rel_name:'',
			rel_amount:''
		};
		$scope.finance.user = $rootScope.name;
		$http({
			method  : 'post',
			url 	: '/project',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.names = response.data;
			console.log($scope.names);
		})

		$scope.getDivision = function(){
			var selectedproj = $scope.finance;
			console.log(selectedproj);
			$http({
				method : 'post',
				url    : '/divs',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedproj)
			}).then(function(response){
				$scope.divisions = response.data;
				console.log($scope.divisions);
			})
		}

		//getting address and tkc 
		$scope.getAddress = function(){
			var selecteddiv = $scope.finance;
			console.log(selecteddiv);
			$http({
				method : 'post',
				url    : '/address',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				for(var i=0;i<response.data.length;i++){	
					$scope.finance.addrs = response.data[i];
				}
			})

			//fetching tkc based of project id and division id 
			$http({
				method : 'post',
				url    : '/tkc_details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				$scope.tkc_details = response.data;
				console.log($scope.tkc_details);
			})
		}

		$scope.getClaim = function(){
			var selectedproj = $scope.finance;
			console.log(selectedproj);
			$http({
				method : 'post',
				url    : '/get_claim',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedproj)
			}).then(function(response){
				$scope.claims = response.data;
				console.log($scope.claims);
			})
		}

		$scope.getReleasedAmt = function(){
			var selectedproj = $scope.finance;
			console.log(selectedproj);
			$http({
				method : 'post',
				url    : '/get_released_amt',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedproj)
			}).then(function(response){
				$scope.amount = response.data;
				var total_claim_amount = $scope.finance.claim.claim_cost;
				var released_amnt = $scope.amount[0].sum;
				if($scope.amount[0].sum == null){
					$scope.released_amt = "0";
					$scope.released_amount =  total_claim_amount - 0;
				}else{
					$scope.released_amt = released_amnt;
					$scope.released_amount =  total_claim_amount - released_amnt;
				}
			})
		}

/*		$scope.pusClaimcost = function(){
			console.log($scope.finance.claim.claim_cost);
		}*/

		$scope.savefinance = function(){
			var form = $scope.finance;
			console.log(form);
			$http({
				method : 'post',
				url : '/saverelease',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(form)
			}).then(function(response){
				toastr.success('Submitted', 'Successfully', {timeOut: 5000});
				$state.go($state.current, {}, {reload: true});
				console.log("successfully submitted finance details");
			})
		}

	}]).

	controller('NodalClaim', ["$scope", "$http", "$window", "$rootScope", "$state", function($scope, $http, $window, $rootScope, $state){
		$scope.finance = {
			pid : '',
			div_id : '',
			tkc_id : '',
			addrs : '',
			claim_name:'',
			claim_amount : '',
			date : '',
			user :''
		};
		$scope.finance.user = $rootScope.name;
		$http({
			method  : 'post',
			url 	: '/project',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.names = response.data;
			console.log($scope.names);
		})

		$scope.getDivision = function(){
			var selectedproj = $scope.finance;
			console.log(selectedproj);
			$http({
				method : 'post',
				url    : '/divs',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedproj)
			}).then(function(response){
				$scope.divisions = response.data;
				console.log($scope.divisions);
			})
		}

		//getting address and tkc 
		$scope.getAddress = function(){
			var selecteddiv = $scope.finance;
			console.log(selecteddiv);
			$http({
				method : 'post',
				url    : '/address',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				for(var i=0;i<response.data.length;i++){	
					$scope.finance.addrs = response.data[i];
				}
			})

			//fetching tkc based of project id and division id 
			$http({
				method : 'post',
				url    : '/tkc_details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				$scope.tkc_details = response.data;
				console.log($scope.tkc_details);
			})
		}

		$scope.savefinance = function(){
			var form = $scope.finance;
			console.log(form);
			$http({
				method : 'post',
				url : '/saveclaim',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(form)
			}).then(function(response){
				toastr.success('Submitted', 'Successfully', {timeOut: 5000});
				$state.go($state.current, {}, {reload: true});
				console.log("successfully submitted finance details");
			})
		}

	}]).
	//Tkc claim 
	controller('TkcClaim', ["$scope", "$http", "$window", "$rootScope", "$state", function($scope, $http, $window, $rootScope, $state){
		$scope.finance = {
			pid : '',
			div_id : '',
			tkc_id : '',
			addrs : '',
			claim_name:'',
			claim_amount : '',
			date : '',
			user :''
		};
		$scope.finance.user = $rootScope.name;
		$http({
			method  : 'post',
			url 	: '/project',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.names = response.data;
			console.log($scope.names);
		})

		$scope.getDivision = function(){
			var selectedproj = $scope.finance;
			console.log(selectedproj);
			$http({
				method : 'post',
				url    : '/divs',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedproj)
			}).then(function(response){
				$scope.divisions = response.data;
				console.log($scope.divisions);
			})
		}

		//getting address and tkc 
		$scope.getAddress = function(){
			var selecteddiv = $scope.finance;
			console.log(selecteddiv);
			$http({
				method : 'post',
				url    : '/address',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				for(var i=0;i<response.data.length;i++){	
					$scope.finance.addrs = response.data[i];
				}
			})

			//fetching tkc based of project id and division id 
			$http({
				method : 'post',
				url    : '/tkc_details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				$scope.tkc_details = response.data;
				console.log($scope.tkc_details);
			})
		}

		$scope.savefinance = function(){
			var form = $scope.finance;
			console.log(form);
			$http({
				method : 'post',
				url : '/saveclaim',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(form)
			}).then(function(response){
				toastr.success('Submitted', 'Successfully', {timeOut: 5000});
				$state.go($state.current, {}, {reload: true});
				console.log("successfully submitted finance details");
			})
		}

	}]).
	//Tkc release 
	controller('TkcRelease', ["$scope", "$http", "$window", "$rootScope", "$state", function($scope, $http, $window, $rootScope, $state){
		$scope.finance = {
			pid : '',
			div_id : '',
			tkc_id : '',
			addrs : '',
			claim_name:'',
			claim_amount : '',
			date : '',
			user :''
		};
		$scope.finance.user = $rootScope.name;
		$http({
			method  : 'post',
			url 	: '/project',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.names = response.data;
			console.log($scope.names);
		})

		$scope.getDivision = function(){
			var selectedproj = $scope.finance;
			console.log(selectedproj);
			$http({
				method : 'post',
				url    : '/divs',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedproj)
			}).then(function(response){
				$scope.divisions = response.data;
				console.log($scope.divisions);
			})
		}

		//getting address and tkc 
		$scope.getAddress = function(){
			var selecteddiv = $scope.finance;
			console.log(selecteddiv);
			$http({
				method : 'post',
				url    : '/address',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				for(var i=0;i<response.data.length;i++){	
					$scope.finance.addrs = response.data[i];
				}
			})

			//fetching tkc based of project id and division id 
			$http({
				method : 'post',
				url    : '/tkc_details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiv)
			}).then(function(response){
				$scope.tkc_details = response.data;
				console.log($scope.tkc_details);
			})
		}

		$scope.savefinance = function(){
			var form = $scope.finance;
			console.log(form);
			$http({
				method : 'post',
				url : '/saveclaim',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(form)
			}).then(function(response){
				toastr.success('Submitted', 'Successfully', {timeOut: 5000});
				$state.go($state.current, {}, {reload: true});
				console.log("successfully submitted finance details");
			})
		}

	}]).
	//tkc approve claim 
	controller('TkcApproveClaim', ["$scope", "$http", "$window", "$rootScope", "$state", function($scope, $http, $window, $rootScope, $state){
		
		$scope.table = true;
		$scope.views = false;

		$scope.view = function(){
			$scope.table = false;
			$scope.views = true;
		}

		$scope.cancel =function(){
			$scope.table = true;
			$scope.views =false;
		}

	}]).
	controller('PendingUser', function($scope, $http, $rootScope)
	{
		$http({
			method : 'post',
			url    : '/unregistered',
			contentType: 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.records = response.data;
			console.log($scope.records);
		})
		$scope.table = true;
		$scope.registration = false;
		$scope.role = [{ name : "Administrator", id : "ADMIN" },
						{ name : "General Manager", id : "GM" },
						{ name : "Cordinator", id : "CR" }];

		$scope.exit = function(){
			$scope.table = true;
			$scope.registration =false
		}
		$scope.create = function(record){
			$scope.registration = true;
			$scope.table = false;
			$scope.addnew = record;	
		}
		$scope.RegisterNewUser = function(){
			var object = $scope.addnew;
			$http({
				method : 'post',
				url    : '/RegisterNewUser',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(object)
			}).then(function(response){
				$scope.registration = false;
				$scope.table = true;
				toastr.success('New user created!', 'Successfully!', {timeOut: 5000});
				console.log("New user created successfully!")
			})
		}
		
	}).
	controller('ExistingUser', function($http, $scope, $rootScope)
	{
		$scope.table = true;
		$scope.prf = false;
		$http({
			method : 'post',
			url    : '/registered',
			contentType: 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.records = response.data;
			console.log($scope.records);
		})
		$scope.profile = function(record){
			$scope.prf = true;
			$scope.table = false;
			$scope.list = record;
		}
		$scope.exit = function(){
			$scope.table =true;
			$scope.prf = false;
		}

/*		$scope.record = {
			enabled_flag : $scope.record.enabled_flag
		};*/
		$scope.disable = function(record){
			var userrec = { 
				userid : record.uid,
				flag 	: record.enabled_flag
			};
			console.log(userrec);
			$http({
				method : 'post',
				url    : '/disable',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(userrec)
			}).then(function(response){
				console.log("User Updated successfully!!");
			})
		}
	}).

	controller('ProjectSummary',['$scope', '$rootScope', '$http', function($scope, $rootScope, $http)
	{	
		$scope.table = true;
		$scope.view_dtls = false;
		var uid = {uid : $rootScope.uid};
		console.log(uid);
		$http({
			method 	 : 'post',
			url 	 : '/proj-details',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(uid)
		}).then(function(response){
		var data = response.data;
		data.forEach(function(item){
			
			var sdate = item.start_date;
			var sdateArray = sdate.split("T")
			var sdateonly = sdateArray[0]
			item.start_date = sdateonly;

			var edate = item.end_date;
			var edateArray = edate.split("T")
			var edateonly = edateArray[0]
			item.end_date = edateonly;
		})
			$scope.proj_list = data;
			console.log($scope.proj_list);
		})

		$scope.view = function(proj){
			$scope.proj_dtls = proj;
			$scope.table = false;
			$scope.view_dtls = true;
			var pid = { "pid": $scope.proj_dtls};
			console.log(pid);
			$http({
				method : 'post',
				url : '/summary_discom',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(pid)
				}).then(function(response){
					$scope.dis_name = response.data;
					console.log($scope.dis_name);
				})
			$http({
				method : 'post',
				url : '/summary_client',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(pid)
				}).then(function(response){
					$scope.client_name = response.data;
					console.log($scope.client_name);
				})
			$http({
				method : 'post',
				url : '/summary_gm',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(pid)
				}).then(function(response){
					$scope.gm_name = response.data;
					console.log($scope.gm_name);
				})
			$http({
				method : 'post',
				url : '/summary_cr',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(pid)
				}).then(function(response){
					$scope.cr_name = response.data;
					console.log($scope.cr_name);
				})
		
		}

		$scope.cancel = function(){
			$scope.table = true;
			$scope.view_dtls = false;
		}
	
	}]).

	controller('Tender',['$scope', '$rootScope', '$http', function($scope, $rootScope, $http)
	{
		$scope.tender = {
			pid:'',
			clientid:'',
			tnd_name:'',
			spec_id:'',
			tnd_spc_no:'',
			tnd_doi:'',
			tnd_ad_date:'',
			remarks:'',	
			t_mapped:'',
			t_mapped_sub_selected:'',

			tnd_dof:'',
			tnd_dos:'',
			tnd_dot:'',
			tnd_dofb:'',
			revise_val:'',
			original_val:'',
			emd:'',
			concerned_person:'',

			user:''
		}
		$scope.tender.user = $rootScope.name;
		var uid = {uid : $rootScope.uid};
		console.log(uid);
		$http({
			method 	 : 'post',
			url 	 : '/proj-details',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(uid)
		}).then(function(response){
			$scope.proj_list = response.data;
			console.log($scope.proj_list);
		})

		$http({
			method 	 : 'post',
			url 	 : '/tender_id',
			contentType: 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			$scope.tenderid = response.data;
			$scope.tender.spec_id = $scope.tenderid.nextval;
			console.log($scope.tenderid);
		})
		$scope.tender_mapped = [{"id":1, "name":"DISCOM"},
								{"id":2, "name":"ZONE"},
								{"id":3, "name":"CIRCLE"},
								{"id":4, "name":"DIVISION"}
								];
		$scope.getClient = function(){
			var client_pid = {"pid":$scope.tender.pid.project_id};
			$http({
				method 	 : 'post',
				url 	 : '/tender_client',
				contentType: 'application/x-www-form-urlencoded',
				data: JSON.stringify(client_pid)
			}).then(function(response){
				$scope.clientid = response.data;
				$scope.tender.clientid = $scope.clientid;
				console.log($scope.clientid);
			})
		}
		$scope.getMapped = function(){
			var pid = {"pid":$scope.tender.pid.project_id};
			console.log(pid);
			if($scope.tender.t_mapped.id == 1){
				$scope.h_name = "Discom Name";
				$http({
					method 	 : 'post',
					url 	 : '/tender_discom',
					contentType: 'application/x-www-form-urlencoded',
					data: JSON.stringify(pid)
				}).then(function(response){
					$scope.t_mapped_sub = response.data;
					console.log($scope.t_mapped_sub);
				})
			}else if($scope.tender.t_mapped.id == 2){
				$scope.h_name = "Zone Name";
				$http({
					method 	 : 'post',
					url 	 : '/tender_zone',
					contentType: 'application/x-www-form-urlencoded',
					data: JSON.stringify(pid)
				}).then(function(response){
					$scope.t_mapped_sub = response.data;
					console.log($scope.t_mapped_sub);
				})
			}else if($scope.tender.t_mapped.id == 3){
				$scope.h_name = "Circle Name";
				$http({
					method 	 : 'post',
					url 	 : '/tender_circle',
					contentType: 'application/x-www-form-urlencoded',
					data: JSON.stringify(pid)
				}).then(function(response){
					$scope.t_mapped_sub = response.data;
					console.log($scope.t_mapped_sub);
				})
			}else{
				$scope.h_name = "Division Name";
				$http({
					method 	 : 'post',
					url 	 : '/tender_division',
					contentType: 'application/x-www-form-urlencoded',
					data: JSON.stringify(pid)
				}).then(function(response){
					$scope.t_mapped_sub = response.data;
					console.log($scope.t_mapped_sub);
				})
			}
		}

		$scope.save = function(){
			var all = $scope.tender;
			console.log(all);
			$http({
				method 	 : 'post',
				url 	 : '/tender_save',
				contentType: 'application/x-www-form-urlencoded',
				data: JSON.stringify(all)
			}).then(function(response){
				console.log("Successfully Inserted Tender Details!");
				//toastr.success('Project Created!', 'Successfully!', {timeOut: 5000})
			})
		}
/*		$scope.new = function(){
			$state.go($state.current, {}, {reload: true});
		}*/
	}]).

	controller('NewProject', ['$scope', '$rootScope', '$http', '$location', '$window', 'ProjectCreation', '$state', function($scope, $rootScope, $http, $location, $window, ProjectCreation, $state)
	{	
		var change1 = $('#change'+1+'');
		$scope.project ={
			table : '',
		    projectname:'', 
		    scheme:'', 
		    pid: '',
		    company: '',
		    discom: '', 
		    zone: [],
		    circle:[],
		    district:[],
		    pro_loc_id:'',
		    pro_loc_sub_id:'',
		    generalmanager:'',
		    cordinator:'',
		    admins:[],
		    logged_in: $rootScope.uid
		};

		$http({
			method : 'post',
			url    : '/GeneralManager',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'	
		}).then(function(response){
			$scope.generalmanager = response.data;
			console.log($scope.generalmanager);			
		})
		$http({
			method : 'post',
			url    : '/cordinator',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'	
		}).then(function(response){
			$scope.cordinator = response.data;	
			console.log($scope.cordinator);		
		})
		$http({
			method : 'post',
			url    : '/admin',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'	
		}).then(function(response){
			console.log(response.data);
			for(var i=0;i<response.data.length;i++){
				$scope.project.admins.push(response.data[i]);
			}
			console.log($scope.project.admins);		
		})

		//Generating pid sequence
		$http({
			method : 'post',
			url    : '/pid',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'	
		}).then(function(response){
			$scope.sequence = response.data.nextval;
			console.log($scope.sequence);
			console.log($scope.sequence);
			$scope.date = new Date();
			var year = $scope.date.getFullYear();
			$scope.project.pid = year+$scope.sequence;
			console.log($scope.project.pid)			
		})

		//Getting list of scheme   
		$scope.schemes = ProjectCreation.getScheme();
		console.log($scope.schemes);
		//$scope.table = ProjectCreation.getCompany();
		//console.log($scope.table);
		    
		$scope.getCompany = function(){
		    $scope.companies = ProjectCreation.getCompany($scope.project.scheme);
		    console.log($scope.companies);
		    var n = $scope.companies.length-1;
		    $scope.project.table = $scope.companies[n].table;
		    console.log($scope.project.table);
		}

		$scope.getDiscom = function(){
			var selectedcompany = $scope.project;
			console.log(selectedcompany);
			$http({
				method : 'post',
				url    : '/discom',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedcompany)
			}).then(function(response){
				$scope.discoms = response.data;
				console.log($scope.discom);
					
			})
		}

		$scope.getZone = function(){
			var selecteddiscom = $scope.project;
			console.log(selecteddiscom);
			$http({
				method : 'post',
				url    : '/zone',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddiscom)
			}).then(function(response){
				$scope.zones = response.data;
				console.log($scope.zone);
			
			})
		}

		$scope.getCircle = function(){
			var selectedzone = $scope.project;
			console.log(selectedzone);
			$http({
				method : 'post',
				url    : '/circle',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedzone)
			}).then(function(response){
				$scope.circles = response.data;
				console.log($scope.circle);
			
			})
		}

		$scope.getDistrict = function(){
			var selectedcircle = $scope.project;
			console.log(selectedcircle);
			$http({
				method : 'post',
				url    : '/district',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedcircle)
			}).then(function(response){
				$scope.districts = response.data;
				console.log($scope.district);
			
			})
		}

		$scope.tkcs = [];
		$scope.division = [];
		$scope.getDivision = function(){
			var selecteddistrict = $scope.project;
			console.log(selecteddistrict);
			$http({
				method : 'post',
				url    : '/division',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddistrict)
			}).then(function(response){
				for(var i=0;i<response.data.length;i++){	
					$scope.division.push(response.data[i]);
					console.log($scope.division);
				
				    if(i == response.data.length - 1){
						initializeDivisionTkcs();
					}
				}
			})
		}
		$http({
			method : 'post',
			url    : '/tkc',
			contentType : 'application/x-www-form-urlencoded',
			dataType : 'json'
		}).then(function(response){
			for(var i=0;i<response.data.length;i++){
				$scope.tkcs.push(response.data[i]);
			}
		})	
	
		//tkc selection based upon division
		$scope.selected = [ ];
		//$scope.s = {};
		function initializeDivisionTkcs() {
			console.log('hello')
			console.log($scope.tkcs);
			console.log($scope.division);
			angular.forEach($scope.division, d => {
			  d.tkcs = angular.copy($scope.tkcs);
			  console.log(d.tkcs);
			})
		};

/*		$scope.show = function(){
    		$scope.showSelected = true;
  		}

		$scope.updateParent = (parent) => {
			parent.selected = !!parent.tkcs.find(d => d.selectedX);
			$scope.selected = ($scope.division.filter(d => d.selected).map(d => {
				return {
				    division_id: d.division_id,
				    division_name: d.division_name,
				    tkcs: d.tkcs.filter(dd => sd.selected).map(dd => {
				      return {
				        tkc_id: sd.tkc_id,
				        tkc_name: sd.tkc_name
				      }
				    })
				}
			}));
			$scope.project.selected = $scope.selected;
			console.log($scope.selected)
		};

	    $scope.getParent = function(index, selectedState){
	      $scope.result = [];
	      var req = {
	        "name": $scope.division[index].division_name, 
	        "id": $scope.division[index].division_id, 
	        "status": selectedState
	      }
	      $scope.result.push(req);
	      console.log("result",$scope.result);
	      return angular.toJson($scope.result);
	      console.log(angular.toJson($scope.result));
	    };*/
		$scope.getParent = function(index, selectedState){
	      var count = 0;
	      var req = {
	        "name": $scope.division[index].division_name, 
	        "id": $scope.division[index].division_id, 
	        "tkcs": selectedState
	      }
	      
	      if($scope.selected.length == 0){
	         $scope.selected.push(req);
	      }else{
	        // particular place insertion
	        for(var i=0; i<$scope.selected.length; i++){
	          if($scope.selected[i].name == $scope.division[index].division_name){
	            $scope.selected[i] = req;
	            count++;
	            break;
	          }
	        }
	        if(count == 0){
	          $scope.selected.push(req);
	        }
	      }
	      
	      console.log("result",$scope.selected);
	      $scope.project.selected = $scope.selected;
	      return angular.toJson($scope.selected);
	    };

/*		$scope.updateParent = (parent) => {
			parent.selected = !!parent.tkcs.find(s => s.selected);
			$scope.selected = ($scope.division.filter(d => d.selected).map(d => {
				return {
				    division_id: d.division_id,
				    division_name: d.division_name,
				    tkcs: d.tkcs.filter(sd => sd.selected).map(sd => {
				      return {
				        tkc_id: sd.tkc_id,
				        tkc_name: sd.tkc_name
				      }
				    })
				}
			}));
			$scope.project.selected = $scope.selected;
			console.log($scope.selected)
		};*/

		$scope.getSubDivision = function(){
			var selecteddivision = $scope.project;
			console.log(selecteddivision);
			$http({
				method : 'post',
				url    : '/sub-division',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddivision)
			}).then(function(response){
				$scope.sub_division = response.data;
				console.log($scope.sub_division);
			
			})
		}

		$scope.getVillage = function(){
			var selecteddistrict = $scope.project;
			console.log(selecteddistrict);
			$http({
				method : 'post',
				url    : '/village',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selecteddistrict)
			}).then(function(response){
				$scope.villages = response.data;
				console.log($scope.villages);
			
			})
		}

		$scope.getMajra = function(){
			var selectedvillage = $scope.project;
			console.log(selectedvillage);
			$http({
				method : 'post',
				url    : '/majra',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedvillage)
			}).then(function(response){
				$scope.majras = response.data;
				console.log($scope.majras);
			})
		}

		//date restriction for start date and end date of project creation
        $scope.maxi = {
         	value: new Date(2015, 3, 22),
         	currentDate: new Date()
       	};
        $scope.mini = {
         	value: new Date(2015, 3, 22),
         	currentDate: new Date()
       	};
		$scope.success = false;
		$scope.proj_creation = true;
			
		//Saving New Project
		$scope.save = function(){

			var NewProject = $scope.project;
			console.log(NewProject);
			//var users = [[$scope.project.pid,$scope.project.generalmanager,'GM01'],['pid','pg007','GM02']];
			$http({
				method : 'post',
				url    : '/saveproject',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(NewProject)
			}).then(function(response){
				console.log("project saved successfully");
				$scope.success = true;
				$scope.proj_creation = false;
				toastr.success('Project Created!', 'Successfully!', {timeOut: 5000})
			})
		}

		$scope.new = function(){
			$state.go($state.current, {}, {reload: true});
		}

	}]).

	controller('FieldEngineer',['$scope', '$rootScope', '$http', '$state', function($scope, $rootScope, $http, $state){
		$scope.table = true;
		$scope.form = false;
		$scope.details = {
			pid : ''
		}
		var uid = {uid : $rootScope.uid};
		console.log(uid);
		$http({
			method 	 : 'post',
			url 	 : '/proj-details',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(uid)
		}).then(function(response){
			$scope.proj_list = response.data;
			console.log($scope.proj_list);
		})
		var ides = {};

		$scope.assign = function(proj){
			$scope.data = proj;
			ides = $scope.data.project_id;
			$scope.schemedtls = {};
			var selectedscheme = $scope.data;
			console.log($scope.data);


/*			$http({
				method : 'post',
				url    : '/scheme-details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedscheme)
			}).then(function(response){
				$scope.schemedtls = response.data;
			})*/

			//division list
			$http({
				method  : 'post',
				url 	: '/field-division',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedscheme)
			}).then(function(response){
				$scope.division_list = response.data;
				console.log("==================");
				console.log($scope.division_list);
				
			})
				$scope.table = false;
				$scope.form = true ;		
		}

		$scope.getFieldEngg = function(){
			var fe_drop = $scope.proj_dtls;
				fe_drop.ids = ides
				console.log(fe_drop);
			$http({
				method : 'post',
				url    : '/field-engineer-details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(fe_drop)
			}).then(function(response){
				$scope.engineer = response.data;
				console.log($scope.engineer)
			})
		}

		$scope.cancel = function(){
			$scope.form = false;
			$scope.table = true;

		}

		$scope.save = function(){
			var details = $scope.proj_dtls;
			details.ids = ides
			details.current_user = $rootScope.uid
			console.log(details) 
			$http({
				method : 'post',
				url : '/sub_field_engineer',
				contentType : 'application/x-www-form-urlencoded',
				data 		: JSON.stringify(details)
				})
				.then(function(response){
					toastr.success('Submitted successfully', 'Done!', {timeOut: 5000});
					$state.go($state.current, {}, {reload: true});
					console.log("successfully submitted project details");
				})
			.catch(function(err){
				toastr.error('Fail submitting data', 'Fail!', {timeOut: 5000});
				console.log(err)
			});
			
		}
	}]).

	controller('Map', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {

		$scope.map = {}
		$scope.userid = "";
		$scope.sessn = "";
		$scope.username = [];
		$http({
				method: 'post',
				url: '/map_username',
				contentType: 'application/x-www-form-urlencoded',
			})
			.then(function(response) {
				console.log(response)
				$scope.map.user_list = response.data
				$scope.map.user_list.forEach(function(item) {
					$scope.userid = item.user_id
					var fname = item.fname+ "" +item.lname
					$scope.username.push(fname)
				})
				console.log($scope.map.user_list)
				console.log("successfully submitted project details");
			})
			.catch(function(err) {
				console.log(err)
			});

		$scope.reset = function() {
			alert('reset')
		}
		$scope.getTripSession = function() {
			var user = $scope.map.uid;
			console.log(user)
			$http({
				method: 'post',
				url: '/map_session',
				contentType: 'application/x-www-form-urlencoded',
				data: user
			})
			.then(function(response) {
				console.log(response)
				$scope.map.sess_list = response.data
				$scope.map.sess_list.forEach(function(item) {
					$scope.sessn = item.session_id
				})

				console.log("---------------------------")
				console.log($scope.map.user_list)
				console.log("successfully submitted project details");
			})
			.catch(function(err) {
				console.log(err)
			});
			
		}


		var location_data = {};

		$scope.save = function(map) {
			var map_details = $scope.map; 
			$scope.latsArr = [];
			$scope.longArr = [];
			$scope.markers = [];
			$scope.markerscount = $scope.markers.length;


		$http({
				method: 'post',
				url: '/user_map_dtls',
				contentType: 'application/x-www-form-urlencoded',
				data: JSON.stringify(map_details)
			})
			.then(function(response) {
				var loc_data = response.data;
				loc_data.forEach(function(item) {

					var lats = item.gps_lat.substr(1).slice(0, -1).split(',');
					var longs = item.gps_long.substr(1).slice(0, -1).split(',');
					for (var x in lats) {
						$scope.latsArr.push({lats: lats[x]})
							console.log($scope.latsArr)
					}
					for (var y in longs) {
						$scope.longArr.push({longs: longs[y]})
							console.log($scope.longArr)
					}

					for (var i = 0; i < $scope.latsArr.length; i++) {

						console.log($scope.latsArr)
						$scope.markers.push({
							latLng: [$scope.latsArr[i].lats + "," + $scope.longArr[i].longs]
						});
						console.log($scope.markers)
						var n = $scope.markers.length - 1;
						$scope.marker = $scope.markers[n].latLng;
						$scope.mark = $scope.markers[0].latLng;
						console.log($scope.marker);
					}
				})
			})
			.catch(function(err) {
				console.log(err)
			});
		}
	}]).

	controller('StoreReview', ['$http', '$scope', '$rootScope',  function($http, $scope, $rootScope)
	{
		alert("STORE REVIEW!!");
	}]).

	//Project Target ctrl
	controller('ProjectTarget', function($http, $scope, $rootScope, $state)
	{
		$scope.target = {
			pid 	:'',
			client  :'',
			discom 	: '',
			zone 	: '',
			circle 	:'',
			district :'',
			division :'',
			tkc 	:'',
			dol 	: '',
			doc 	:'',
			sanctioned_cost :'',
			approved_cost 	:'',
			cable_type 		:'',
			progress_type 	:'',
			trg_cost 		:''
		};
		$scope.table = true;
		$scope.add_new = false;
		$scope.submittedtarget = false;		
		//$scope.review_sec = false;
		
		var uid = {uid : $rootScope.uid};
		console.log(uid);
		$http({
			method 	 : 'post',
			url 	 : '/proj-details',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(uid)
		}).then(function(response){
		var data = response.data;
		data.forEach(function(item){
			
			var sdate = item.start_date;
			var sdateArray = sdate.split("T")
			var sdateonly = sdateArray[0]
			item.start_date = sdateonly;

			var edate = item.end_date;
			var edateArray = edate.split("T")
			var edateonly = edateArray[0]
			item.end_date = edateonly;
		})
			$scope.proj_list = data;
			console.log($scope.proj_list);
		})
		$scope.add = function(proj){
			$scope.add_new 	  = true ;
			$scope.table 	  = false;
			$scope.submittedtarget = false;

			$scope.data = proj;
			$scope.target.pid = $scope.data;
			var pid = $scope.target;
			console.log(pid);			
			$http({
				method : 'post',
				url    : '/trg_discom',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(pid)
			}).then(function(response){
				$scope.discoms = response.data;
			})
			$http({
				method : 'post',
				url    : '/trg_company',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(pid)
			}).then(function(response){
				$scope.target.client = response.data;
				console.log($scope.target.client);
			})

		$scope.open = function(proj){
			$scope.submittedtarget = true;
			$scope.edit_trg = false;
			$scope.table_trg = true;
			$scope.table = false;
			$scope.add_new  = false ;
			$scope.data = proj;
		}

		$scope.edit = function(proj){
			$scope.submittedtarget = true;
			$scope.edit_trg = true;
			$scope.table_trg = false;
			$scope.table = false;
			$scope.add_new  = false ;
			$scope.data = proj;
		}

		$scope.cancel = function(){
			//$scope.dat = false;
			$scope.table = true;
			$scope.add_new = false;
			$scope.submittedtarget = false;

			//$scope.review_sec = false;
		}
			
/*			$http({
				method : 'post',
				url    : '/scheme-details',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedscheme)
			}).then(function(response){
				$scope.schemedtls = response.data;
			})

			//tkc contractor list
			$http({
				method  : 'post',
				url 	: '/tkcs',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(selectedscheme)
			}).then(function(response){
				$scope.tkc_list = response.data;
				console.log("===============");
				console.log($scope.tkc_list);
			})*/

			//progress type
			$http({
				method  : 'post',
				url 	: '/progress',
				contentType : 'application/x-www-form-urlencoded'
				//data : JSON.stringify(selectedscheme)
			}).then(function(response){
				$scope.progress_list = response.data;
				console.log("===============");
				console.log($scope.progress_list);
			})

			//cable type
			$http({
				method  : 'post',
				url 	: '/cable',
				contentType : 'application/x-www-form-urlencoded'
				//data : JSON.stringify(selectedscheme)
			}).then(function(response){
				$scope.cable_list = response.data;
				console.log("===============");
				console.log($scope.cable_list);
			})
		}

		$scope.getZone = function(){
			var discom = $scope.target;
			console.log(discom)
			$http({
				method : 'post',
				url    : '/trg_zone',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(discom)
			}).then(function(response){
				$scope.zones = response.data;
			})
		}

		$scope.getCircle = function(){
			var zone = $scope.target;
			console.log(zone)
			$http({
				method : 'post',
				url    : '/trg_circle',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(zone)
			}).then(function(response){
				$scope.circles = response.data;
			})
		}

		$scope.getDistrict = function(){
			var circle = $scope.target;
			console.log(circle)
			$http({
				method : 'post',
				url    : '/trg_district',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(circle)
			}).then(function(response){
				$scope.districts = response.data;
			})
		}

		$scope.getDivision = function(){
			var district = $scope.target;
			console.log(district)
			$http({
				method : 'post',
				url    : '/trg_division',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(district)
			}).then(function(response){
				$scope.divisions = response.data;
			})
		}

		$scope.getTkc = function(){
			var division = $scope.target;
			console.log(division)
			$http({
				method : 'post',
				url    : '/trg_tkc',
				contentType : 'application/x-www-form-urlencoded',
				data : JSON.stringify(division)
			}).then(function(response){
				$scope.tkcs = response.data;
			})
		}

		$scope.reviewtask = function(){
			$scope.review_sec = true;
			$scope.form = false;
			/*$scope.target_details = false;*/
			var review_details = {}
			var details = $scope.proj_dtls;
			console.log("==========");
			console.log($rootScope.uid);
			console.log("==========");

			review_details.project_id = details.ids.project_id;
			review_details.scheme_id = details.ids.scheme_id;
			review_details.user_id = $rootScope.uid;
			$http({
				method : 'post',
				url : '/review',
				contentType : 'application/x-www-form-urlencoded',
				data 		: JSON.stringify(review_details)
				})
				.then(function(response){
					$scope.review_list = response.data;
					console.log($scope.review_list);
					console.log("successfully submitted project details");
				})
			.catch(function(err){
				console.log(err)
			});


		}
		$scope.editReview = function(){
				$scope.table = false;
				$scope.review_sec = false;
				$scope.form = true;
				$scope.progress_list = {}
				$scope.cable_list = {}
				$scope.tkc_list = {}
				$scope.proj_dtls = {}
				console.log(review)
				$scope.data = review;
				var selected_reiew = $scope.data;
					selected_reiew.cable_type_id = details.cable.cable_type_id

				$http({
					method : 'post',
					url    : '/review-details',
					contentType : 'application/x-www-form-urlencoded',
					data : JSON.stringify(selected_reiew)
				}).then(function(response){
					
					$scope.progress_list = details.progress;
					$scope.cable_list = details.cable;
					$scope.tkc_list = details.tkc;
					$scope.proj_dtls = response.data;
					

				})
			}

		$scope.save = function(){
			var details = $scope.target;
			details.user = $rootScope.name;
			console.log(details) 
			$http({
				method : 'post',
				url : '/save_target',
				contentType : 'application/x-www-form-urlencoded',
				data 		: JSON.stringify(details)
				})
				.then(function(response){
					console.log("successfully submitted project details");
					toastr.success('Project Target Submitted Successfully', 'For '+details.pid.project_id, {timeOut: 5000})
					$state.go($state.current, {}, {reload: true});
				})
			.catch(function(err){
				toastr.error('Failed T o Submit Project Target', 'Inconceivable!', {timeOut: 5000})
				console.log(err)
			});
			
		}
	}).
	controller('ProjectProgress', ["$scope", "$rootScope", "$http", function($scope, $rootScope, $http) {
	var uid = {
		uid: $rootScope.uid
	};

	$scope.progress = {};
	$scope.progress.user_id = $rootScope.uid;
	$scope.progress_details;

	$scope.static_values =[{name_id : 1,name : "TKC"},{name_id : 2,name : "FE"}]

	$http({
		method: 'post',
		url: '/progress_dtls',
		contentType: 'application/x-www-form-urlencoded',
		data: JSON.stringify(uid)
	}).then(function(response) {
		var data = response.data;
		data.forEach(function(item){
			
			var sdate = item.start_date;
			var sdateArray = sdate.split("T")
			var sdateonly = sdateArray[0]
			item.start_date = sdateonly;

			var edate = item.end_date;
			var edateArray = edate.split("T")
			var edateonly = edateArray[0]
			item.end_date = edateonly;
		})
		$scope.proj = data;
		console.log($scope.proj);
	})
	$scope.table = true;
	$scope.apprv = false;
	$scope.apprv_table = false;
	$scope.progress_selected_details = false;

	$scope.open = function(pro) {
		console.log("================================")
		console.log(pro)
		$scope.progress.project_id = pro.project_id;
		$scope.progress.div_town_id = pro.div_town_id;
		$scope.table = false;
		$scope.apprv = true;
		$scope.apprv_table = false;
		$scope.progress_selected_details = false;
		$scope.data = pro;
		var sdate = $scope.data.start_date;
		var sdateArray = sdate.split("T")
		var sdateonly = sdateArray[0]
		//delete $scope.data.start_date
		$scope.data.start_date = sdateonly;

		console.log($scope.data.start_date)

		var edate = $scope.data.end_date;
		var edateArray = edate.split("T")
		var edateonly = edateArray[0]
		$scope.data.end_date = edateonly;

		var pid = {
			pid: $scope.data.project_id,
			user_id: $rootScope.uid
		};
		console.log(pid);
		$http({
			method: 'post',
			url: '/progress_open',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(pid)
		}).then(function(response) {
			var dataArray = response.data;
			dataArray.forEach(function(item){
				$scope.progress_details = item;	
			})
			
			console.log($scope.progress_details);
		})
	}

	$scope.getProgressData = function(){
		var query_param = {};
		console.log($scope.progress.progress_opts)
	    query_param.name_id = $scope.progress.progress_opts.name_id;
	    query_param.project_id = $scope.progress.project_id;
	    query_param.user_id = $scope.progress.user_id;
	    query_param.div_town_id = $scope.progress.div_town_id
		console.log(query_param)
		$http({
			method: 'post',
			url: '/progress_select_opts',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(query_param)
		}).then(function(response) {

			$scope.dataArr = response.data;
			$scope.dataArr.forEach(function(item){
				if(item.tkc_id){ 
					var obj = item
					obj.name = obj.tkc_name
					delete obj.tkc_name;
					obj.id = obj.tkc_id;
					delete obj.tkc_id;
					/*$scope.progress_dropdown = obj;*/
					console.log($scope.dataArr)
		


				}else{
					var obj = item
					obj.name = obj.username
					delete obj.username;
					obj.id = obj.uid
					delete obj.uid;		
					/*$scope.progress_dropdown.item_name = obj;*/
					console.log($scope.dataArr)
				}
			})
			console.log(response.data);
		})
	}


	$scope.show = function() {
		var req_data ={};
		req_data.pid = $scope.progress.project_id;
		console.log(req_data)
		$http({
			method: 'post',
			url: '/progress_tlbData',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(req_data)
		}).then(function(response) {
			console.log(response);
			if(response.data.code != 0 ){
				
				$scope.tbl_data =response.data;
				$scope.table = false;
				$scope.apprv = false;
				$scope.apprv_table = true;
				$scope.progress_selected_details = false;

			}else{
				console.log('no data')
				toastr.error('No data available to assign ', 'Failed!', {timeOut: 1000});
			}
			
		}).catch(function(error) {
			toastr.error('No data available to assign', 'Failed!', {timeOut: 1000});
			
    	});
		
		console.log('valuessss')
		console.log($scope.progress.progress_opts)
		console.log($scope.progress.secondlist)
		
	}

	$scope.smry_data = {};
	$scope.progress_img;
	$scope.img_name;
	$scope.view = function(item) {
		var req_data ={};
		req_data.pid = item.project_id;
		req_data.seq_id = item.seq_id;
		console.log(req_data)
		$http({
			method: 'post',
			url: '/progress_smryData',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(req_data)
		}).then(function(response) {
			 var arr = response.data;
			 console.log(response)
			 arr.forEach(function(item){
			 	console.log(item)
			 	$scope.smry_data =item
			 	if($scope.smry_data.image_path == "" || $scope.smry_data.image_path == undefined ){
			 		$scope.smry_data.image_path = "Sorry ! No image available for this Project"
			 	}else{
			 		$scope.progress_img = "assets/Progressimages"+"/"+item.image_path
			 		var imageArr = item.image_path.split('/');
			 		$scope.img_name = imageArr[1]
			 	}
			 })
			 if($scope.progress_img == "" || $scope.progress_img == undefined ){
			 	$scope.progress_images = false;	
			 }else{
			 	$scope.progress_images = true;
			 }

			$scope.table = false;
			$scope.apprv = false;
			$scope.apprv_table = false;
			$scope.progress_selected_details = true;
			console.log(response);


		}).catch(function(error) {
      
				toastr.error('No data Found', 'Failed!', {timeOut: 1000});
			
    	});

    	/*$http({
			method: 'post',
			url: '/progress_image',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(req_data)
		}).then(function(response) {
			console.log(response.data)
			var rawImg = response.data
			var b64Img = btoa(rawImg);
			console.log(b64Img)
			$scope.progress_img = 'data:image/jpg;base64,'+b64Img
		}).catch(function(error) {
      
				toastr.error('No data Found', 'Failed!', {timeOut: 1000});
			
    	});*/

		
		
	}

	$scope.approve = function() {
		var approve_data = {}
		approve_data = $scope.smry_data
		$http({
			method: 'post',
			url: '/approve_progressData',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(approve_data)
		}).then(function(response) {
			$scope.table = true;
			$scope.apprv = false;
			$scope.apprv_table = false;
			$scope.progress_selected_details = false;	
			console.log(response);
			toastr.success('Updated approved details ', 'Successfully!', {timeOut: 5000});
		})
	}

	$scope.reject = function() {
		$scope.table = false;
		$scope.apprv = false;
		$scope.apprv_table = false;
		$scope.progress_selected_details = false;
		$scope.progress_edit_details = true;
		$http({
				method: 'post',
				url: '/reject_comments',
				contentType: 'application/x-www-form-urlencoded',
			}).then(function(response) {	
				console.log(response);
				$scope.comments_list = response.data;
			})
	}

	$scope.submit = function() {
		var reject_data = {}
		reject_data = $scope.smry_data;
		$http({
			method: 'post',
			url: '/reject_progressData',
			contentType: 'application/x-www-form-urlencoded',
			data: JSON.stringify(reject_data)
		}).then(function(response) {
			$scope.table = false;
			$scope.apprv = false;
			$scope.apprv_table = false;
			$scope.progress_selected_details = false;
			$scope.progress_edit_details = true;
			console.log(response);
			toastr.success('Updated reject details', 'Successfully!', {timeOut: 5000});
		})
	}

	$scope.cancel = function() {
		$scope.smry_data = {};
		$scope.tbl_data = [];
		$scope.dataArr = [];
		$scope.table = true;
		$scope.apprv = false;
		$scope.apprv_table = false;
		$scope.progress_selected_details = false;
	}
}]).
	controller('RegisterCtrl', function($scope, $rootScope) {
		$rootScope.isRegisterPage = true;
		$rootScope.isLoginPage = false;
		$rootScope.isLightLoginPage = false;
		$rootScope.isLockscreenPage = false;
		$rootScope.isMainPage = false;
		$rootScope.isForgotPage = false;
	}).
	controller('ForgotCtrl', function($scope, $rootScope) {
		$rootScope.isForgotPage = true;
		$rootScope.isRegisterPage = false;
		$rootScope.isLoginPage = false;
		$rootScope.isLightLoginPage = false;
		$rootScope.isLockscreenPage = false;
		$rootScope.isMainPage = false;
	}).
	controller('LoginLightCtrl', function($scope, $rootScope) {
		$rootScope.isLoginPage = true;
		$rootScope.isLightLoginPage = true;
		$rootScope.isLockscreenPage = false;
		$rootScope.isMainPage = false;
	}).
	controller('LockscreenCtrl', function($scope, $rootScope) {
		$rootScope.isLoginPage = false;
		$rootScope.isLightLoginPage = false;
		$rootScope.isLockscreenPage = true;
		$rootScope.isMainPage = false;
	}).
	controller('MainCtrl',  function($scope, $rootScope, $location, $layout, $layoutToggles, $pageLoadingBar, Fullscreen)
	{
		$rootScope.isLoginPage        = false;
		$rootScope.isLightLoginPage   = false;
		$rootScope.isLockscreenPage   = false;
		$rootScope.isMainPage         = true;

		$rootScope.layoutOptions = {
			horizontalMenu: {
				isVisible		: false,
				isFixed			: true,
				minimal			: false,
				clickToExpand	: false,

				isMenuOpenMobile: false
			},
			sidebar: {
				isVisible		: true,
				isCollapsed		: false,
				toggleOthers	: true,
				isFixed			: true,
				isRight			: false,

				isMenuOpenMobile: false,

				// Added in v1.3
				userProfile		: true
			},
			chat: {
				isOpen			: false,
			},
			settingsPane: {
				isOpen			: false,
				useAnimation	: true
			},
			container: {
				isBoxed			: false
			},
			skins: {
				sidebarMenu		: '',
				horizontalMenu	: '',
				userInfoNavbar	: ''
			},
			pageTitles: true,
			userInfoNavVisible	: false
		};

		$layout.loadOptionsFromCookies(); // remove this line if you don't want to support cookies that remember layout changes


		$scope.updatePsScrollbars = function()
		{
			var $scrollbars = jQuery(".ps-scrollbar:visible");

			$scrollbars.each(function(i, el)
			{
				if(typeof jQuery(el).data('perfectScrollbar') == 'undefined')
				{
					jQuery(el).perfectScrollbar();
				}
				else
				{
					jQuery(el).perfectScrollbar('update');
				}
			})
		};


		// Define Public Vars
		public_vars.$body = jQuery("body");


		// Init Layout Toggles
		$layoutToggles.initToggles();


		// Other methods
		$scope.setFocusOnSearchField = function()
		{
			public_vars.$body.find('.search-form input[name="s"]').focus();

			setTimeout(function(){ public_vars.$body.find('.search-form input[name="s"]').focus() }, 100 );
		};


		// Watch changes to replace checkboxes
		$scope.$watch(function()
		{
			cbr_replace();
		});

		// Watch sidebar status to remove the psScrollbar
		$rootScope.$watch('layoutOptions.sidebar.isCollapsed', function(newValue, oldValue)
		{
			if(newValue != oldValue)
			{
				if(newValue == true)
				{
					public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('destroy')
				}
				else
				{
					public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar({wheelPropagation: public_vars.wheelPropagation});
				}
			}
		});


		// Page Loading Progress (remove/comment this line to disable it)
		$pageLoadingBar.init();

		$scope.showLoadingBar = showLoadingBar;
		$scope.hideLoadingBar = hideLoadingBar;


		// Set Scroll to 0 When page is changed
		$rootScope.$on('$stateChangeStart', function()
		{
			var obj = {pos: jQuery(window).scrollTop()};

			TweenLite.to(obj, .25, {pos: 0, ease:Power4.easeOut, onUpdate: function()
			{
				$(window).scrollTop(obj.pos);
			}});
		});


		// Full screen feature added in v1.3
		$scope.isFullscreenSupported = Fullscreen.isSupported();
		$scope.isFullscreen = Fullscreen.isEnabled() ? true : false;

		$scope.goFullscreen = function()
		{
			if (Fullscreen.isEnabled())
				Fullscreen.cancel();
			else
				Fullscreen.all();

			$scope.isFullscreen = Fullscreen.isEnabled() ? true : false;
		}

	}).
	controller('SidebarMenuCtrl', function($scope, $rootScope, $menuItems, $timeout, $location, $state, $layout)
	{

		// Menu Items
		var $sidebarMenuItems = $menuItems.instantiate();

		$scope.menuItems = $sidebarMenuItems.prepareSidebarMenu().getAll();

		// Set Active Menu Item
		$sidebarMenuItems.setActive( $location.path() );

		$rootScope.$on('$stateChangeSuccess', function()
		{
			$sidebarMenuItems.setActive($state.current.name);
		});

		// Trigger menu setup
		public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
		$timeout(setup_sidebar_menu, 1);

		ps_init(); // perfect scrollbar for sidebar
	}).
	controller('HorizontalMenuCtrl', function($scope, $rootScope, $menuItems, $timeout, $location, $state)
	{
		var $horizontalMenuItems = $menuItems.instantiate();

		$scope.menuItems = $horizontalMenuItems.prepareHorizontalMenu().getAll();

		// Set Active Menu Item
		$horizontalMenuItems.setActive( $location.path() );

		$rootScope.$on('$stateChangeSuccess', function()
		{
			$horizontalMenuItems.setActive($state.current.name);

			$(".navbar.horizontal-menu .navbar-nav .hover").removeClass('hover'); // Close Submenus when item is selected
		});

		// Trigger menu setup
		$timeout(setup_horizontal_menu, 1);
	}).
	controller('SettingsPaneCtrl', function($rootScope)
	{
		// Define Settings Pane Public Variable
		public_vars.$settingsPane = public_vars.$body.find('.settings-pane');
		public_vars.$settingsPaneIn = public_vars.$settingsPane.find('.settings-pane-inner');
	}).
	controller('ChatCtrl', function($scope, $element)
	{
		var $chat = jQuery($element),
			$chat_conv = $chat.find('.chat-conversation');

		$chat.find('.chat-inner').perfectScrollbar(); // perfect scrollbar for chat container


		// Chat Conversation Window (sample)
		$chat.on('click', '.chat-group a', function(ev)
		{
			ev.preventDefault();

			$chat_conv.toggleClass('is-open');

			if($chat_conv.is(':visible'))
			{
				$chat.find('.chat-inner').perfectScrollbar('update');
				$chat_conv.find('textarea').autosize();
			}
		});

		$chat_conv.on('click', '.conversation-close', function(ev)
		{
			ev.preventDefault();

			$chat_conv.removeClass('is-open');
		});
	}).
	controller('UIModalsCtrl', function($scope, $rootScope, $modal, $sce)
	{
		// Open Simple Modal
		$scope.openModal = function(modal_id, modal_size, modal_backdrop)
		{
			$rootScope.currentModal = $modal.open({
				templateUrl: modal_id,
				size: modal_size,
				backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
			});
		};

		// Loading AJAX Content
		$scope.openAjaxModal = function(modal_id, url_location)
		{
			$rootScope.currentModal = $modal.open({
				templateUrl: modal_id,
				resolve: {
					ajaxContent: function($http)
					{
						return $http.get(url_location).then(function(response){
							$rootScope.modalContent = $sce.trustAsHtml(response.data);
						}, function(response){
							$rootScope.modalContent = $sce.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
						});
					}
				}
			});

			$rootScope.modalContent = $sce.trustAsHtml('Modal content is loading...');
		}
	}).
	controller('PaginationDemoCtrl', function($scope)
	{
		$scope.totalItems = 64;
		$scope.currentPage = 4;

		$scope.setPage = function (pageNo)
		{
			$scope.currentPage = pageNo;
		};

		$scope.pageChanged = function()
		{
			console.log('Page changed to: ' + $scope.currentPage);
		};

		$scope.maxSize = 5;
		$scope.bigTotalItems = 175;
		$scope.bigCurrentPage = 1;
	}).
	controller('LayoutVariantsCtrl', function($scope, $layout, $cookies)
	{
		$scope.opts = {
			sidebarType: null,
			fixedSidebar: null,
			sidebarToggleOthers: null,
			sidebarVisible: null,
			sidebarPosition: null,

			horizontalVisible: null,
			fixedHorizontalMenu: null,
			horizontalOpenOnClick: null,
			minimalHorizontalMenu: null,

			sidebarProfile: null
		};

		$scope.sidebarTypes = [
			{value: ['sidebar.isCollapsed', false], text: 'Expanded', selected: $layout.is('sidebar.isCollapsed', false)},
			{value: ['sidebar.isCollapsed', true], text: 'Collapsed', selected: $layout.is('sidebar.isCollapsed', true)},
		];

		$scope.fixedSidebar = [
			{value: ['sidebar.isFixed', true], text: 'Fixed', selected: $layout.is('sidebar.isFixed', true)},
			{value: ['sidebar.isFixed', false], text: 'Static', selected: $layout.is('sidebar.isFixed', false)},
		];

		$scope.sidebarToggleOthers = [
			{value: ['sidebar.toggleOthers', true], text: 'Yes', selected: $layout.is('sidebar.toggleOthers', true)},
			{value: ['sidebar.toggleOthers', false], text: 'No', selected: $layout.is('sidebar.toggleOthers', false)},
		];

		$scope.sidebarVisible = [
			{value: ['sidebar.isVisible', true], text: 'Visible', selected: $layout.is('sidebar.isVisible', true)},
			{value: ['sidebar.isVisible', false], text: 'Hidden', selected: $layout.is('sidebar.isVisible', false)},
		];

		$scope.sidebarPosition = [
			{value: ['sidebar.isRight', false], text: 'Left', selected: $layout.is('sidebar.isRight', false)},
			{value: ['sidebar.isRight', true], text: 'Right', selected: $layout.is('sidebar.isRight', true)},
		];

		$scope.horizontalVisible = [
			{value: ['horizontalMenu.isVisible', true], text: 'Visible', selected: $layout.is('horizontalMenu.isVisible', true)},
			{value: ['horizontalMenu.isVisible', false], text: 'Hidden', selected: $layout.is('horizontalMenu.isVisible', false)},
		];

		$scope.fixedHorizontalMenu = [
			{value: ['horizontalMenu.isFixed', true], text: 'Fixed', selected: $layout.is('horizontalMenu.isFixed', true)},
			{value: ['horizontalMenu.isFixed', false], text: 'Static', selected: $layout.is('horizontalMenu.isFixed', false)},
		];

		$scope.horizontalOpenOnClick = [
			{value: ['horizontalMenu.clickToExpand', false], text: 'No', selected: $layout.is('horizontalMenu.clickToExpand', false)},
			{value: ['horizontalMenu.clickToExpand', true], text: 'Yes', selected: $layout.is('horizontalMenu.clickToExpand', true)},
		];

		$scope.minimalHorizontalMenu = [
			{value: ['horizontalMenu.minimal', false], text: 'No', selected: $layout.is('horizontalMenu.minimal', false)},
			{value: ['horizontalMenu.minimal', true], text: 'Yes', selected: $layout.is('horizontalMenu.minimal', true)},
		];

		$scope.chatVisibility = [
			{value: ['chat.isOpen', false], text: 'No', selected: $layout.is('chat.isOpen', false)},
			{value: ['chat.isOpen', true], text: 'Yes', selected: $layout.is('chat.isOpen', true)},
		];

		$scope.boxedContainer = [
			{value: ['container.isBoxed', false], text: 'No', selected: $layout.is('container.isBoxed', false)},
			{value: ['container.isBoxed', true], text: 'Yes', selected: $layout.is('container.isBoxed', true)},
		];

		$scope.sidebarProfile = [
			{value: ['sidebar.userProfile', false], text: 'No', selected: $layout.is('sidebar.userProfile', false)},
			{value: ['sidebar.userProfile', true], text: 'Yes', selected: $layout.is('sidebar.userProfile', true)},
		];

		$scope.resetOptions = function()
		{
			$layout.resetCookies();
			window.location.reload();
		};

		var setValue = function(val)
		{
			if(val != null)
			{
				val = eval(val);
				$layout.setOptions(val[0], val[1]);
			}
		};

		$scope.$watch('opts.sidebarType', setValue);
		$scope.$watch('opts.fixedSidebar', setValue);
		$scope.$watch('opts.sidebarToggleOthers', setValue);
		$scope.$watch('opts.sidebarVisible', setValue);
		$scope.$watch('opts.sidebarPosition', setValue);

		$scope.$watch('opts.horizontalVisible', setValue);
		$scope.$watch('opts.fixedHorizontalMenu', setValue);
		$scope.$watch('opts.horizontalOpenOnClick', setValue);
		$scope.$watch('opts.minimalHorizontalMenu', setValue);

		$scope.$watch('opts.chatVisibility', setValue);

		$scope.$watch('opts.boxedContainer', setValue);

		$scope.$watch('opts.sidebarProfile', setValue);
	}).
	controller('ThemeSkinsCtrl', function($scope, $layout)
	{
		var $body = jQuery("body");

		$scope.opts = {
			sidebarSkin: $layout.get('skins.sidebarMenu'),
			horizontalMenuSkin: $layout.get('skins.horizontalMenu'),
			userInfoNavbarSkin: $layout.get('skins.userInfoNavbar')
		};

		$scope.skins = [
			{value: '', 			name: 'Default'			,	palette: ['#2c2e2f','#EEEEEE','#FFFFFF','#68b828','#27292a','#323435']},
			{value: 'aero', 		name: 'Aero'			,	palette: ['#558C89','#ECECEA','#FFFFFF','#5F9A97','#558C89','#255E5b']},
			{value: 'navy', 		name: 'Navy'			,	palette: ['#2c3e50','#a7bfd6','#FFFFFF','#34495e','#2c3e50','#ff4e50']},
			{value: 'facebook', 	name: 'Facebook'		,	palette: ['#3b5998','#8b9dc3','#FFFFFF','#4160a0','#3b5998','#8b9dc3']},
			{value: 'turquoise', 	name: 'Truquoise'		,	palette: ['#16a085','#96ead9','#FFFFFF','#1daf92','#16a085','#0f7e68']},
			{value: 'lime', 		name: 'Lime'			,	palette: ['#8cc657','#ffffff','#FFFFFF','#95cd62','#8cc657','#70a93c']},
			{value: 'green', 		name: 'Green'			,	palette: ['#27ae60','#a2f9c7','#FFFFFF','#2fbd6b','#27ae60','#1c954f']},
			{value: 'purple', 		name: 'Purple'			,	palette: ['#795b95','#c2afd4','#FFFFFF','#795b95','#27ae60','#5f3d7e']},
			{value: 'white', 		name: 'White'			,	palette: ['#FFFFFF','#666666','#95cd62','#EEEEEE','#95cd62','#555555']},
			{value: 'concrete', 	name: 'Concrete'		,	palette: ['#a8aba2','#666666','#a40f37','#b8bbb3','#a40f37','#323232']},
			{value: 'watermelon', 	name: 'Watermelon'		,	palette: ['#b63131','#f7b2b2','#FFFFFF','#c03737','#b63131','#32932e']},
			{value: 'lemonade', 	name: 'Lemonade'		,	palette: ['#f5c150','#ffeec9','#FFFFFF','#ffcf67','#f5c150','#d9a940']},
		];

		$scope.$watch('opts.sidebarSkin', function(val)
		{
			if(val != null)
			{
				$layout.setOptions('skins.sidebarMenu', val);

				$body.attr('class', $body.attr('class').replace(/\sskin-[a-z]+/)).addClass('skin-' + val);
			}
		});

		$scope.$watch('opts.horizontalMenuSkin', function(val)
		{
			if(val != null)
			{
				$layout.setOptions('skins.horizontalMenu', val);

				$body.attr('class', $body.attr('class').replace(/\shorizontal-menu-skin-[a-z]+/)).addClass('horizontal-menu-skin-' + val);
			}
		});

		$scope.$watch('opts.userInfoNavbarSkin', function(val)
		{
			if(val != null)
			{
				$layout.setOptions('skins.userInfoNavbar', val);

				$body.attr('class', $body.attr('class').replace(/\suser-info-navbar-skin-[a-z]+/)).addClass('user-info-navbar-skin-' + val);
			}
		});
	}).
	// Added in v1.3
	controller('FooterChatCtrl', function($scope, $element)
	{
		$scope.isConversationVisible = false;

		$scope.toggleChatConversation = function()
		{
			$scope.isConversationVisible = ! $scope.isConversationVisible;

			if($scope.isConversationVisible)
			{
				setTimeout(function()
				{
					var $el = $element.find('.ps-scrollbar');

					if($el.hasClass('ps-scroll-down'))
					{
						$el.scrollTop($el.prop('scrollHeight'));
					}

					$el.perfectScrollbar({
						wheelPropagation: false
					});

					$element.find('.form-control').focus();

				}, 300);
			}
		}
	});