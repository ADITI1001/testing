'use strict';

var app = angular.module('xenon-app', [
	'ngCookies',

	'ui.router',
	'ui.bootstrap',

	'oc.lazyLoad',

	'xenon.controllers',
	'xenon.directives',
	'xenon.factory',
	'xenon.services',

	// Added in v1.3
	'FBAngular'
	//'xenon.authservice'
]);

app.run(function()
{
	// Page Loading Overlay
	public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

	jQuery(window).load(function()
	{
		public_vars.$pageLoadingOverlay.addClass('loaded');
	})
});

app.config(function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, ASSETS){

	//$urlRouterProvider.otherwise('/register');
	$urlRouterProvider.otherwise('/login');
/*if($cookies["loggedIn"])
	{
	    $urlRouterProvider.otherwise('/app/dashboard-variant-1');
	}
	else
	{
	    $urlRouterProvider.otherwise('/app/login');
	}*/


	$stateProvider.
		// Main Layout Structure
		state('app', {
			abstract: true,
			url: '/app',
			templateUrl: appHelper.templatePath('layout/app-body'),
			controller: function($rootScope){
/*				$scope.username = userProfile.fname;
				console.log($scope.username);*/
				$rootScope.isLoginPage        = true;
				$rootScope.isLightLoginPage   = false;
				$rootScope.isLockscreenPage   = false;
				$rootScope.isMainPage         = true;
			},
			resolve: {
				access: ["Access", function (Access) { return Access.isAuthenticated(); }],
				userProfile: "UserProfile",
			}
		}).

		// Dashboards
		state('app.dashboard-variant-1', {
			url: '/dashboard-variant-1',
			templateUrl: appHelper.templatePath('dashboards/variant-1'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
						ASSETS.extra.toastr,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.dashboard-variant-2', {
			url: '/dashboard-variant-2',
			templateUrl: appHelper.templatePath('dashboards/variant-2'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.dashboard-variant-3', {
			url: '/dashboard-variant-3',
			templateUrl: appHelper.templatePath('dashboards/variant-3'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
						ASSETS.maps.vectorMaps,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.variant-4', {
			url: '/variant-4',
			templateUrl: appHelper.templatePath('variant-4'),
			controller: 'Dashboard',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.icons.meteocons,
						ASSETS.maps.vectorMaps,
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		// Update Highlights
		state('app.update-highlights', {
			url: '/update-highlights',
			templateUrl: appHelper.templatePath('update-highlights'),
		}).

		// Layouts
		state('app.layout-and-skins', {
			url: '/layout-and-skins',
			templateUrl: appHelper.templatePath('layout-and-skins'),
		}).

		// UI Elements
		state('app.ui-panels', {
			url: '/ui-panels',
			templateUrl: appHelper.templatePath('ui/panels'),
		}).
		state('app.ui-buttons', {
			url: '/ui-buttons',
			templateUrl: appHelper.templatePath('ui/buttons')
		}).
		state('app.ui-tabs-accordions', {
			url: '/ui-tabs-accordions',
			templateUrl: appHelper.templatePath('ui/tabs-accordions')
		}).
		state('app.ui-modals', {
			url: '/ui-modals',
			templateUrl: appHelper.templatePath('ui/modals'),
			controller: 'UIModalsCtrl'
		}).
		state('app.ui-breadcrumbs', {
			url: '/ui-breadcrumbs',
			templateUrl: appHelper.templatePath('ui/breadcrumbs')
		}).
		state('app.ui-blockquotes', {
			url: '/ui-blockquotes',
			templateUrl: appHelper.templatePath('ui/blockquotes')
		}).
		state('app.ui-progress-bars', {
			url: '/ui-progress-bars',
			templateUrl: appHelper.templatePath('ui/progress-bars')
		}).
		state('app.ui-navbars', {
			url: '/ui-navbars',
			templateUrl: appHelper.templatePath('ui/navbars')
		}).
		state('app.ui-alerts', {
			url: '/ui-alerts',
			templateUrl: appHelper.templatePath('ui/alerts')
		}).
		state('app.ui-pagination', {
			url: '/ui-pagination',
			templateUrl: appHelper.templatePath('ui/pagination')
		}).
		state('app.ui-typography', {
			url: '/ui-typography',
			templateUrl: appHelper.templatePath('ui/typography')
		}).
		state('app.ui-other-elements', {
			url: '/ui-other-elements',
			templateUrl: appHelper.templatePath('ui/other-elements')
		}).

		// Widgets
		state('app.widgets', {
			url: '/widgets',
			templateUrl: appHelper.templatePath('widgets'),
			resolve: {
				deps: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.maps.vectorMaps,
						ASSETS.icons.meteocons
					]);
				}
			}
		}).

		// Mailbox
		state('app.mailbox-inbox', {
			url: '/mailbox-inbox',
			templateUrl: appHelper.templatePath('mailbox/inbox'),
		}).
		state('app.mailbox-compose', {
			url: '/mailbox-compose',
			templateUrl: appHelper.templatePath('mailbox/compose'),
			resolve: {
				bootstrap: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
					]);
				},
				bootstrapWysihtml5: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.bootstrapWysihtml5,
					]);
				},
			}
		}).
		state('app.mailbox-message', {
			url: '/mailbox-message',
			templateUrl: appHelper.templatePath('mailbox/message'),
		}).

		// Tables
		state('app.tables-basic', {
			url: '/tables-basic',
			templateUrl: appHelper.templatePath('tables/basic'),
		}).
		state('app.tables-responsive', {
			url: '/tables-responsive',
			templateUrl: appHelper.templatePath('tables/responsive'),
			resolve: {
				deps: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.tables.rwd,
					]);
				}
			}
		}).
		state('app.tables-datatables', {
			url: '/tables-datatables',
			templateUrl: appHelper.templatePath('tables/datatables'),
			resolve: {
				deps: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.tables.datatables,
					]);
				},
			}
		}).

		// Forms
/*		state('app.forms-native', {
			url: '/forms-native',
			templateUrl: appHelper.templatePath('forms/native-elements'),
		}).
		state('app.forms-advanced', {
			url: '/forms-advanced',
			templateUrl: appHelper.templatePath('forms/advanced-plugins'),
			resolve: {
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				},
				typeahead: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.typeahead,
					]);
				},
				datepicker: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.datepicker,
					]);
				},
				timepicker: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.timepicker,
					]);
				},
				daterangepicker: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.moment,
						ASSETS.forms.daterangepicker,
					]);
				},
				colorpicker: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.colorpicker,
					]);
				},
			}
		}).
		state('app.forms-wizard', {
			url: '/forms-wizard',
			templateUrl: appHelper.templatePath('forms/form-wizard'),
			resolve: {
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
			},
		}).
		state('app.forms-validation', {
			url: '/forms-validation',
			templateUrl: appHelper.templatePath('forms/form-validation'),
			resolve: {
				jQueryValidate: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			},
		}).
		state('app.forms-input-masks', {
			url: '/forms-input-masks',
			templateUrl: appHelper.templatePath('forms/input-masks'),
			resolve: {
				inputmask: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.inputmask,
					]);
				},
			},
		}).
		state('app.forms-file-upload', {
			url: '/forms-file-upload',
			templateUrl: appHelper.templatePath('forms/file-upload'),
			resolve: {
				dropzone: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.dropzone,
					]);
				},
			}
		}).
		state('app.forms-wysiwyg', {
			url: '/forms-wysiwyg',
			templateUrl: appHelper.templatePath('forms/wysiwyg'),
			resolve: {
				bootstrap: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
					]);
				},
				bootstrapWysihtml5: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.bootstrapWysihtml5,
					]);
				},
				uikit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.uikit.base,
						ASSETS.uikit.codemirror,
						ASSETS.uikit.marked,
					]);
				},
				uikitHtmlEditor: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.uikit.htmleditor,
					]);
				},
			}
		}).
		state('app.forms-sliders', {
			url: '/forms-sliders',
			templateUrl: appHelper.templatePath('forms/sliders'),
			resolve: {
				sliders: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
					]);
				},
			},
		}).
		state('app.forms-icheck', {
			url: '/forms-icheck',
			templateUrl: appHelper.templatePath('forms/icheck'),
			resolve: {
				iCheck: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.icheck,
					]);
				},
			}
		}).*/

		// Extra
		state('app.extra-icons-font-awesome', {
			url: '/extra-icons-font-awesome',
			templateUrl: appHelper.templatePath('extra/icons-font-awesome'),
			resolve: {
				fontAwesome: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
						ASSETS.extra.tocify,
					]);
				},
			}
		}).
		state('app.extra-icons-linecons', {
			url: '/extra-icons-linecons',
			templateUrl: appHelper.templatePath('extra/icons-linecons'),
			resolve: {
				linecons: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
						ASSETS.extra.tocify,
					]);
				},
			}
		}).
		state('app.extra-icons-elusive', {
			url: '/extra-icons-elusive',
			templateUrl: appHelper.templatePath('extra/icons-elusive'),
			resolve: {
				elusive: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
						ASSETS.extra.tocify,
						ASSETS.icons.elusive,
					]);
				},
			}
		}).
		state('app.extra-icons-meteocons', {
			url: '/extra-icons-meteocons',
			templateUrl: appHelper.templatePath('extra/icons-meteocons'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
						ASSETS.extra.tocify,
						ASSETS.icons.meteocons,
					]);
				},
			}
		}).
		state('app.extra-profile', {
			url: '/extra-profile',
			templateUrl: appHelper.templatePath('extra/profile'),
			resolve: {
				profile: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
						ASSETS.icons.elusive,
					]);
				},
			}
		}).
		state('app.extra-timeline', {
			url: '/extra-timeline',
			templateUrl: appHelper.templatePath('extra/timeline'),
			resolve: {
				timeline: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
					]);
				},
			}
		}).
		state('app.extra-timeline-centered', {
			url: '/extra-timeline-centered',
			templateUrl: appHelper.templatePath('extra/timeline-centered'),
			resolve: {
				elusive: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
					]);
				},
			}
		}).
		state('app.extra-calendar', {
			url: '/extra-calendar',
			templateUrl: appHelper.templatePath('extra/calendar'),
			resolve: {
				fullCalendar: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
						ASSETS.core.moment,
						ASSETS.extra.fullCalendar,
					]);
				},
			}
		}).
		state('app.extra-gallery', {
			url: '/extra-gallery',
			templateUrl: appHelper.templatePath('extra/gallery'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
					]);
				},
			}
		}).
		state('app.extra-notes', {
			url: '/extra-notes',
			templateUrl: appHelper.templatePath('extra/notes'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.xenonLib.notes,
					]);
				},
			}
		}).
		state('app.extra-image-crop', {
			url: '/extra-image-crop',
			templateUrl: appHelper.templatePath('extra/image-cropper'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.cropper,
					]);
				},
			}
		}).
		state('app.extra-portlets', {
			url: '/extra-portlets',
			templateUrl: appHelper.templatePath('extra/portlets'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
					]);
				},
			}
		}).
		state('app.extra-search', {
			url: '/extra-search',
			templateUrl: appHelper.templatePath('extra/search')
		}).
		state('app.extra-invoice', {
			url: '/extra-invoice',
			templateUrl: appHelper.templatePath('extra/invoice')
		}).
		state('app.extra-page-404', {
			url: '/extra-page-404',
			templateUrl: appHelper.templatePath('extra/page-404')
		}).
		state('app.extra-tocify', {
			url: '/extra-tocify',
			templateUrl: appHelper.templatePath('extra/tocify'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
						ASSETS.extra.tocify,
					]);
				},
			}
		}).
		state('app.extra-loading-progress', {
			url: '/extra-loading-progress',
			templateUrl: appHelper.templatePath('extra/loading-progress')
		}).
		state('app.extra-notifications', {
			url: '/extra-notifications',
			templateUrl: appHelper.templatePath('extra/notifications'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
					]);
				},
			}
		}).
		state('app.extra-nestable-lists', {
			url: '/extra-nestable-lists',
			templateUrl: appHelper.templatePath('extra/nestable-lists'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.uikit.base,
						ASSETS.uikit.nestable,
					]);
				},
			}
		}).
		state('app.extra-scrollable', {
			url: '/extra-scrollable',
			templateUrl: appHelper.templatePath('extra/scrollable')
		}).
		state('app.extra-blank-page', {
			url: '/extra-blank-page',
			templateUrl: appHelper.templatePath('extra/blank-page')
		}).
		state('app.extra-maps-google', {
			url: '/extra-maps-google',
			templateUrl: appHelper.templatePath('extra/maps-google'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
					]);
				},
			}
		}).
		state('app.extra-maps-advanced', {
			url: '/extra-maps-advanced',
			templateUrl: appHelper.templatePath('extra/maps-advanced'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
					]);
				},
			}
		}).
		state('app.extra-maps-vector', {
			url: '/extra-maps-vector',
			templateUrl: appHelper.templatePath('extra/maps-vector'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.maps.vectorMaps,
					]);
				},
			}
		}).

		// Members
		state('app.extra-members-list', {
			url: '/extra-members-list',
			templateUrl: appHelper.templatePath('extra/members-list')
		}).
		state('app.extra-members-add', {
			url: '/extra-members-add',
			templateUrl: appHelper.templatePath('extra/members-add'),
			resolve: {
				datepicker: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.datepicker,
						ASSETS.forms.multiSelect,
						ASSETS.forms.select2,
					]);
				},
				//sssss
			}
		}).

		// Charts
		state('app.charts-variants', {
			url: '/charts-variants',
			templateUrl: appHelper.templatePath('charts/bars'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.charts-range-selector', {
			url: '/charts-range-selector',
			templateUrl: appHelper.templatePath('charts/range'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.charts-sparklines', {
			url: '/charts-sparklines',
			templateUrl: appHelper.templatePath('charts/sparklines'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.charts-gauges', {
			url: '/charts-gauges',
			templateUrl: appHelper.templatePath('charts/gauges'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.charts-bar-gauges', {
			url: '/charts-bar-gauges',
			templateUrl: appHelper.templatePath('charts/bar-gauges'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.charts-linear-gauges', {
			url: '/charts-linear-gauges',
			templateUrl: appHelper.templatePath('charts/gauges-linear'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				},
			}
		}).
		state('app.charts-map-charts', {
			url: '/charts-map-charts',
			templateUrl: appHelper.templatePath('charts/map'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
						ASSETS.charts.dxVMWorld,
					]);
				},
			}
		}).

		// Logins and Lockscreen
		state('login', {
			url: '/login',
			templateUrl: appHelper.templatePath('login'),
			controller: 'LoginCtrl',
			resolve: {
				
				access: ["Access", function (Access) { return Access.isAnonymous(); }],
				userProfile: "UserProfile",
				
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		}).

		state('app.forbidden', {
			url: '/forbidden',
			templateUrl: appHelper.templatePath('forbidden'),
			//controller: 'Forbidden',
			resolve: {
				
				//access: ["Access", function (Access) { return Access.isAnonymous(); }],
				//userProfile: "UserProfile",
				
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		}).

		state('logout', {
			url: '/login',
			controller: 'Logout'
		}).

		// Registeration for new user
		state('register', {
			url: '/register',
			templateUrl: appHelper.templatePath('register'),
			//controller: 'RegisterCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		}).
		// forgot password for new user
		state('forgot', {
			url: '/forgot',
			templateUrl: appHelper.templatePath('forgot'),
			/*controller: 'ForgotCtrl',*/
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
					]);
				},
			}
		}).
		// resetPassword 
		state('resetpassword', {
			url: '/resetpassword',
			templateUrl: appHelper.templatePath('resetpassword'),
			//controller: 'RegisterCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
					]);
				}
			}
		}).
		//nodal release 
		state('app.financial-nodal-release', {
			url: '/financial-nodal-release',
			templateUrl: appHelper.templatePath('financial/nodal-release'),
			controller : 'NodalRelease',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).
		//nodal claim
		state('app.financial-nodal-claim', {
			url: '/financial-nodal-claim',
			templateUrl: appHelper.templatePath('financial/nodal-claim'),
			controller : 'NodalClaim',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).
		//tkc claim module
		state('app.financial-tkc-claim', {
			url: '/financial-tkc-claim',
			templateUrl: appHelper.templatePath('financial/tkc-claim'),
			controller : 'TkcClaim',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).
		//tkc release module
		state('app.financial-tkc-release', {
			url: '/financial-tkc-release',
			templateUrl: appHelper.templatePath('financial/tkc-release'),
			controller : 'TkcRelease',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).
		//Approve tkc claim
		state('app.financial-tkc-approve_claim', {
			url: '/financial-tkc-approve_claim',
			templateUrl: appHelper.templatePath('financial/tkc-approve_claim'),
			controller : 'TkcApproveClaim',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).
		//my-page 
		state('app.my-page', {
			url: '/my-page',
			templateUrl: appHelper.templatePath('my-page'),
			controller : 'MyPage',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
			}
		}).
		//User Admin- pending user
		state('app.user-admin-pending-user', {
			url: '/user-admin-pending-user',
			templateUrl: appHelper.templatePath('user-admin/pending-user'),
			controller : 'PendingUser',
			resolve: {
				access: ["Access", function (Access) { return Access.hasRole("ADMIN"); }],
				userProfile: "UserProfile",
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
			}
		}).
		//user admin existing user
		state('app.user-admin-existing-user', {
			url: '/user-admin-existing-user',
			templateUrl: appHelper.templatePath('user-admin/existing-user'),
			controller : 'ExistingUser',
			resolve: {

				access: ["Access", function (Access) { return Access.hasRole("ADMIN"); }],
				userProfile: "UserProfile",

				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
			}
		}).

		//field engineer
		state('app.field-engineer', {
			url: '/field-engineer',
			templateUrl: appHelper.templatePath('field-engineer'),
			controller : 'FieldEngineer',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		//Maps
		state('app.map', {
			url: '/map',
			templateUrl: appHelper.templatePath('map'),
			controller : 'Map',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						ASSETS.forms.jQueryValidate,
						
					]);  
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,

					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		//Maps
		state('app.store-store_review', {
			url: '/store-store_review',
			templateUrl: appHelper.templatePath('store/store_review'),
			controller : 'StoreReview',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.googleMapsLoader,
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						ASSETS.forms.jQueryValidate,
						
					]);  
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,

					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		//project summary 
		state('app.proj_summary', {
			url: '/proj_summary',
			templateUrl: appHelper.templatePath('proj_summary'),
			controller : 'ProjectSummary',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		//tender & tkc details  
		state('app.tender', {
			url: '/tender',
			templateUrl: appHelper.templatePath('tender'),
			controller : 'Tender',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		//New project creation 
		state('app.new-project', {
			url: '/new-project',
			templateUrl: appHelper.templatePath('new-project'),
			controller : 'NewProject',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		//Projects Details 
		state('app.proj-details-proj-details', {
			url: '/proj-details-proj-details',
			templateUrl: appHelper.templatePath('proj-details/proj-details'),
			controller : 'ProjectTarget',
			resolve: {
				
				access: ["Access", function (Access) { return Access.isAuthenticated(); }],
				userProfile: "UserProfile",

				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

				//Projects Details 
		state('app.proj-details-proj-progress', {
			url: '/proj-details-proj-progress',
			templateUrl: appHelper.templatePath('proj-details/proj-progress'),
			controller : 'ProjectProgress',
			resolve: {
				
				access: ["Access", function (Access) { return Access.isAuthenticated(); }],
				userProfile: "UserProfile",
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
						ASSETS.forms.formWizard,
						//'app/js/my-page/my-page.js',
						//'my-other-dir/other.js'
					]);
				},
				fwDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.inputmask,
						ASSETS.forms.multiSelect,
						ASSETS.forms.datepicker,
						ASSETS.forms.selectboxit,
						ASSETS.forms.formWizard,
					]);
				},
				formWizard: function($ocLazyLoad){
					return $ocLazyLoad.load([
					]);
				},
				jqui: function($ocLazyLoad){
					return $ocLazyLoad.load({
						files: ASSETS.core.jQueryUI
					});
				},
				select2: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.select2,
					]);
				},
				selectboxit: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.selectboxit,
					]);
				},
				tagsinput: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.tagsinput,
					]);
				},
				multiSelect: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.multiSelect,
					]);
				}
			}
		}).

		state('login-light', {
			url: '/login-light',
			templateUrl: appHelper.templatePath('login-light'),
			controller: 'LoginLightCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			}
		}).
		state('lockscreen', {
			url: '/lockscreen',
			templateUrl: appHelper.templatePath('lockscreen'),
			controller: 'LockscreenCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		});
}).

/*run(function($rootScope, $state, $http, AUTH_EVENTS, AuthService){
  
  $rootScope.$on('$stateChangeStart', function(event, next){
    
    if(next.name !== 'login'){
      var authorizedRoles = next.data.authorizedRoles;
      
      if(!AuthService.isAuthorized(authorizedRoles)){
        
        event.preventDefault();
        
        if(AuthService.isAuthenticated())
           $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        else
           $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
           
        $state.go('login');
        alert('Access Denied');
        
      }
    }
       
  })
  
});*/

run(["$rootScope", "Access", "$state", "$log", function ($rootScope, Access, $state, $log) {

	$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
		switch (error) {

		case Access.UNAUTHORIZED:
		  $state.go("login");
		  break;

		case Access.FORBIDDEN:
		  $state.go("app.forbidden");
		  break;

		default:
		  $log.warn("$stateChangeError event catched");
		  break;

		}
	});
/*
  $rootScope.$on('$stateChangeSuccess', function (event, toState) {
		if (toState.name === 'logout') {
			//$Access.logoutUser();
			$state.go('login');
		}  
   });
*/
}]);

app.constant('ASSETS', {
	'core': {
		'bootstrap': appHelper.assetPath('js/bootstrap.min.js'), // Some plugins which do not support angular needs this

		'jQueryUI': [
			appHelper.assetPath('js/jquery-ui/jquery-ui.min.js'),
			appHelper.assetPath('js/jquery-ui/jquery-ui.structure.min.css'),
		],

		'moment': appHelper.assetPath('js/moment.min.js'),

		'googleMapsLoader': appHelper.assetPath('js/angular-google-maps/load-google-maps.js')
	},

	'charts': {

		'dxGlobalize': appHelper.assetPath('js/devexpress-web-14.1/js/globalize.min.js'),
		'dxCharts': appHelper.assetPath('js/devexpress-web-14.1/js/dx.chartjs.js'),
		'dxVMWorld': appHelper.assetPath('js/devexpress-web-14.1/js/vectormap-data/world.js'),
	},

	'xenonLib': {
		notes: appHelper.assetPath('js/xenon-notes.js'),
	},

	'maps': {

		'vectorMaps': [
			appHelper.assetPath('js/jvectormap/jquery-jvectormap-1.2.2.min.js'),
			appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-world-mill-en.js'),
			appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-it-mill-en.js'),
		],
	},

	'icons': {
		'meteocons': appHelper.assetPath('css/fonts/meteocons/css/meteocons.css'),
		'elusive': appHelper.assetPath('css/fonts/elusive/css/elusive.css'),
	},

	'tables': {
		'rwd': appHelper.assetPath('js/rwd-table/js/rwd-table.min.js'),

		'datatables': [
			appHelper.assetPath('js/datatables/dataTables.bootstrap.css'),
			appHelper.assetPath('js/datatables/datatables-angular.js'),
		],

	},

	'forms': {

		'select2': [
			appHelper.assetPath('js/select2/select2.css'),
			appHelper.assetPath('js/select2/select2-bootstrap.css'),

			appHelper.assetPath('js/select2/select2.min.js'),
		],

		'daterangepicker': [
			appHelper.assetPath('js/daterangepicker/daterangepicker-bs3.css'),
			appHelper.assetPath('js/daterangepicker/daterangepicker.js'),
		],

		'colorpicker': appHelper.assetPath('js/colorpicker/bootstrap-colorpicker.min.js'),

		'selectboxit': appHelper.assetPath('js/selectboxit/jquery.selectBoxIt.js'),

		'tagsinput': appHelper.assetPath('js/tagsinput/bootstrap-tagsinput.min.js'),

		'datepicker': appHelper.assetPath('js/datepicker/bootstrap-datepicker.js'),

		'timepicker': appHelper.assetPath('js/timepicker/bootstrap-timepicker.min.js'),

		'inputmask': appHelper.assetPath('js/inputmask/jquery.inputmask.bundle.js'),

		'formWizard': appHelper.assetPath('js/formwizard/jquery.bootstrap.wizard.min.js'),

		'jQueryValidate': appHelper.assetPath('js/jquery-validate/jquery.validate.min.js'),

		'dropzone': [
			appHelper.assetPath('js/dropzone/css/dropzone.css'),
			appHelper.assetPath('js/dropzone/dropzone.min.js'),
		],

		'typeahead': [
			appHelper.assetPath('js/typeahead.bundle.js'),
			appHelper.assetPath('js/handlebars.min.js'),
		],

		'multiSelect': [
			appHelper.assetPath('js/multiselect/css/multi-select.css'),
			appHelper.assetPath('js/multiselect/js/jquery.multi-select.js'),
		],

		'icheck': [
			appHelper.assetPath('js/icheck/skins/all.css'),
			appHelper.assetPath('js/icheck/icheck.min.js'),
		],

		'bootstrapWysihtml5': [
			appHelper.assetPath('js/wysihtml5/src/bootstrap-wysihtml5.css'),
			appHelper.assetPath('js/wysihtml5/wysihtml5-angular.js')
		],
	},

	'uikit': {
		'base': [
			appHelper.assetPath('js/uikit/uikit.css'),
			appHelper.assetPath('js/uikit/css/addons/uikit.almost-flat.addons.min.css'),
			appHelper.assetPath('js/uikit/js/uikit.min.js'),
		],

		'codemirror': [
			appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.js'),
			appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.css'),
		],

		'marked': appHelper.assetPath('js/uikit/vendor/marked.js'),
		'htmleditor': appHelper.assetPath('js/uikit/js/addons/htmleditor.min.js'),
		'nestable': appHelper.assetPath('js/uikit/js/addons/nestable.min.js'),
	},

	'extra': {
		'tocify': appHelper.assetPath('js/tocify/jquery.tocify.min.js'),

		'toastr': appHelper.assetPath('js/toastr/toastr.min.js'),

		'fullCalendar': [
			appHelper.assetPath('js/fullcalendar/fullcalendar.min.css'),
			appHelper.assetPath('js/fullcalendar/fullcalendar.min.js'),
		],

		'cropper': [
			appHelper.assetPath('js/cropper/cropper.min.js'),
			appHelper.assetPath('js/cropper/cropper.min.css'),
		]
	}
});
